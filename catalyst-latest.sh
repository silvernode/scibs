#!/bin/bash
#####################################
##Title: Scibs - Simple Catalyst Installing Bash Script
##Author: Mollusk
##Contributors: Pincasts
##Version: 0.0.1-4
##Date: Aug 28 2013
#####################################

function PauseScreen(){
read -n 1 -p "Press any key to return to Main Menu..."
}

function RunDepends(){
arch=$(uname -p)

if [ "$arch" == "x86_64" ]; then
  echo
  echo "Installing dependencies with 32 libs..."
  echo
  sleep 1

  sudo apt-get -y install build-essential cdbs fakeroot dh-make debhelper debconf libstdc++6 dkms libqtgui4 wget execstack libelfg0 dh-modaliases unzip
  sudo apt-get -y install ia32-libs-multiarch i386 lib32gcc1 libc6-i386 

elif [ "$arch" == "i686" ]; then
  echo "Installing dependencies..."
  echo
  sleep 1
  sudo apt-get -y install build-essential cdbs fakeroot dh-make debhelper debconf libstdc++6 dkms libqtgui4 wget execstack libelfg0 dh-modaliases unzip
  echo

elif [ "$arch" == "i386" ]; then
  echo "Installing dependencies..."
  echo
  sleep 1
  sudo apt-get -y install build-essential cdbs fakeroot dh-make debhelper debconf libstdc++6 dkms libqtgui4 wget execstack libelfg0 dh-modaliases unzip
  echo
else
  return true
fi
}

function chkDir(){
if [ -d "/tmp/Build" ]; then
  sudo rm -R /tmp/Build
  mkdir /tmp/Build

elif [ ! -d "/tmp/Build" ]; then
  mkdir /tmp/Build

else
  return
fi
}

function RunInstall(){
pkgVer="13-4"
ubuntuVer=$(lsb_release -sc)

chkDir
cd /tmp/Build
echo "Downloading the latest Catalyst drivers..."
echo
wget http://www2.ati.com/drivers/linux/amd-driver-installer-catalyst-$pkgVer-linux-x86.x86_64.zip
echo "Extracting the zip file..."
echo
unzip amd-driver-installer-catalyst-$pkgVer-linux-x86.x86_64.zip
echo "Generating packages for Ubuntu "$ubuntuVer
echo
sudo sh *.run --buildpkg Ubuntu/$ubuntuVer
echo "Installing generated packages..."
echo
sudo dpkg -i *.deb
echo "Modifying needed sections of xorg.conf file..."
echo
sudo aticonfig --initial
echo "Catalyst 'should' now be fully installed!"
echo "Cleaning up left over files..."
cd ~
sudo rm -R /tmp/Build
echo
echo "***NOTE***" 
echo
echo "You must reboot to load the driver"
echo "Once rebooted you can check if the driver is installed"
echo
echo "by selecting: 'Verify' from the main menu"
echo


}

function UninstallDrivers(){
echo "Removing currently install Catalyst drivers..."
echo
sleep 1

sudo sh /usr/share/ati/fglrx-uninstall.sh
sudo apt-get -y remove --purge fglrx fglrx_* fglrx-amdcccle* fglrx-dev*

echo
echo "Restoring xorg.conf file.."
sudo mv /etc/X11/xorg.conf.original-0 /etc/X11/xorg.conf
echo
}

function ChkIfInstalled(){

if [ -d "/lib/fglrx" ]; then
  UninstallDrivers
  RunDepends
  RunInstall
  echo
  PauseScreen 
elif [ ! -d "/lib/fglrx" ]; then
  RunDepends
  RunInstall
  echo
  PauseScreen
fi

}

function AskReboot(){
echo
echo -n "Are you sure you want to reboot?[y/n]: "
read answer

if [ "$answer" == "y" ]; then
  sudo reboot

elif [ "$answer" == "n" ]; then
  MainMenu

else
  echo "Please press 'y' for yes or 'n' for no"
  echo "<<< Returning to main menu"
  sleep 2
  MainMenu
fi

}

function VerifyDriver(){
clear
echo "::::::Driver Info::::::"
echo
echo ":::NOTE:::"
echo "If you do not see the word 'mesa' below,"
echo "then you should be running the Catalyst driver"
echo
fglrxinfo
echo
PauseScreen

}

function MainMenu(){
while true
do
  clear
  echo ":::::: Scibs v0.0.1-4 ::::::"
  echo "Latest AMD Display Driver"
  echo
  echo
  echo "[1] - Install"
  echo "[2] - Reboot"
  echo "[3] - Verify (requires reboot)"
  echo "[4] - Uninstall"
  echo
  echo "[q] - Quit"
  echo
  echo -n "Please choose an option: "
  read choice

  if [ "$choice" == "1" ]; then
    ChkIfInstalled

  elif [ "$choice" == "2" ]; then
    AskReboot

  elif [ "$choice" == "3" ]; then
    VerifyDriver

  elif [ "$choice" == "4" ]; then
    UninstallDrivers
    PauseScreen

  elif [ "$choice" == "h" ]; then
    ViewHelp | more

  elif [ "$choice" == "q" ]; then
    exit 0

  else
    echo "You have pressed an incorrect key!"
    sleep 3

  fi
    
    

done
}

MainMenu









